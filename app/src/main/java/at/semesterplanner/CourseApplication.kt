package at.semesterplanner

import android.app.Application
import at.semesterplanner.data.CourseDatabase
import at.semesterplanner.data.CourseRepository

class CourseApplication : Application() {

    val database by lazy { CourseDatabase.getDatabase(this) }
    val repository by lazy { CourseRepository(database.courseDataItem(), applicationContext) }
}