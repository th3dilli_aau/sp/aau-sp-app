package at.semesterplanner.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.semesterplanner.*
import at.semesterplanner.data.CourseData
import kotlin.math.absoluteValue


class CalendarFragment : Fragment(), GestureDetector.OnGestureListener {
    private val weekdays = listOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    private lateinit var gestureDetector: GestureDetector
    private lateinit var weekday: TextView
    private var dayCounter = 0
    private var dayChangeHandled = false
    private var allcourses = CourseData()
    private val model: CourseViewModel by viewModels {
        CourseViewModelFactory((context?.applicationContext as CourseApplication).repository)
    }
    private lateinit var courseAdapter: CourseAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_calendar, container, false)
        courseAdapter = CourseAdapter(context, true)
        gestureDetector = GestureDetector(context, this)

        weekday = view.findViewById(R.id.weekDayID)
        val sharedPref =
            requireContext().applicationContext.getSharedPreferences(
                requireContext().applicationContext.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )

        dayCounter = sharedPref.getInt(requireContext().applicationContext.getString(R.string.weekday), 0)
        model.courses.observe(viewLifecycleOwner) { courses ->
            allcourses.clear()
            allcourses.addAll(courses)
            update()
        }

        view.setOnTouchListener { v, event ->
            v.performClick()
            gestureDetector.onTouchEvent(event)
        }
        val listView = view.findViewById<RecyclerView>(R.id.listViewID)
        listView.setOnTouchListener { v, event ->
            v.performClick()
            gestureDetector.onTouchEvent(event)
        }
        listView.adapter = courseAdapter
        listView.layoutManager = LinearLayoutManager(context)
        listView.setHasFixedSize(true)
        (listView.adapter as CourseAdapter).notifyFragment = {
            update()
        }
        return view
    }

    override fun onPause() {
        super.onPause()
        val sharedPref =
            requireContext().applicationContext.getSharedPreferences(
                requireContext().applicationContext.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )
        with(sharedPref.edit()) {
            putInt(
                requireContext().applicationContext.getString(R.string.weekday),
                dayCounter
            )
            apply()
        }
    }

    override fun onDown(e: MotionEvent?): Boolean {
        dayChangeHandled = false
        return true
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {

        if (e2 != null && e1 != null) {
            val diff = e1.x - e2.x
            if (diff.absoluteValue >= requireView().width * 0.4) {
                if (diff >= 0) {
                    if (!dayChangeHandled) {
                        dayCounter++
                        dayChangeHandled = true
                        if (dayCounter >= 7) {
                            dayCounter = 0
                        }
                        update()
                        return true
                    }
                } else {
                    if (!dayChangeHandled) {
                        dayChangeHandled = true
                        dayCounter--
                        if (dayCounter <= -1) {
                            dayCounter = 6
                        }
                        update()
                        return true
                    }
                }
            }
        }
        return false
    }

    override fun onLongPress(e: MotionEvent?) {

    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        return false
    }

    private fun update() {
        weekday.text = weekdays[dayCounter]
        val filtered = CourseData()
        filtered.addAll(allcourses.filter { course -> course.added && course.weekDay == weekdays[dayCounter] })
        filtered.sortBy {
            it.startTime.toFloat()
        }
        for (a in filtered) {
            a.isOverlapping = false
        }
        for (f in filtered) {
            for (s in filtered) {
                if (f.id != s.id) {
                    val fstarttime = f.startTime.toFloat()
                    val sstarttime = s.startTime.toFloat()
                    val fendtime = f.endTime.toFloat()
                    val sendtime = s.endTime.toFloat()
                    if (fstarttime < sstarttime && fendtime > sstarttime ||
                        sstarttime < fstarttime && sendtime > fstarttime ||
                        sstarttime == fstarttime && sendtime == fendtime
                    ) {
                        f.isOverlapping = true
                        s.isOverlapping = true
                    }
                }
            }
        }
        courseAdapter.update(filtered)
    }
}