package at.semesterplanner.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.semesterplanner.*

class SearchFragment : Fragment() {
    private val model: CourseViewModel by viewModels {
        CourseViewModelFactory((context?.applicationContext as CourseApplication).repository)
    }
    private lateinit var courseAdapter: CourseAdapter
    lateinit var recycleView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_search, container, false)
        model.courses.observe(viewLifecycleOwner) { courses ->
            courseAdapter.update(courses)
        }
        courseAdapter = CourseAdapter(context, false)
        recycleView = view.findViewById(R.id.recycler_view)
        recycleView.adapter = courseAdapter
        recycleView.layoutManager = LinearLayoutManager(context)
        recycleView.setHasFixedSize(true)

        val searchView = view.findViewById<SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                (recycleView.adapter as CourseAdapter).filter.filter(newText)
                return false
            }
        })

        searchView.setOnCloseListener {
            (recycleView.adapter as CourseAdapter).filter.filter("")
            false
        }
        return view
    }
}