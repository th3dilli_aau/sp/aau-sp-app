package at.semesterplanner

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.RecyclerView
import at.semesterplanner.data.CourseData
import at.semesterplanner.data.CourseDataItem
import at.semesterplanner.databinding.CourseContainerBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*


class CourseAdapter(val context: Context?, private val showOverlapping: Boolean) :
    RecyclerView.Adapter<CourseAdapter.ViewHolder>(), Filterable {
    private var filterText: String = ""
    val allCourses = CourseData()
    val courseList = CourseData()
    var notifyFragment: (() -> Unit)? = null

    inner class ViewHolder(val binding: CourseContainerBinding) : RecyclerView.ViewHolder(binding.root)


    private val listener:BtnClickListener = object :BtnClickListener {
        override fun onClick(view: ViewHolder, position: Int) {
            val course = courseList[position]
            if (course.added) {
                view.binding.buttonID.setImageResource(R.drawable.ic_baseline_add_24)
            } else {
                view.binding.buttonID.setImageResource(R.drawable.ic_baseline_delete_24)
            }
            course.added = !course.added
            if (!course.added) {
                course.isOverlapping = false
            }


            notifyFragment?.invoke()
            CoroutineScope(Dispatchers.IO).launch {
                if (context != null) {
                    (context.applicationContext as CourseApplication).repository.update(course)
                }
            }
        }
    }

    interface BtnClickListener {
        fun onClick(view: ViewHolder, position: Int)
    }

    fun update(courses: List<CourseDataItem>) {
        courseList.clear()
        courseList.addAll(courses)
        allCourses.clear()
        allCourses.addAll(courses)
        filter.filter(filterText)
        if (filterText == "") {
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = CourseContainerBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            val currentCourse = courseList[position]
            binding.tvTitle.text = currentCourse.title

            binding.tvTitle.text = currentCourse.title
            binding.tvDate.text = currentCourse.getTime()
            binding.tvTeacher.text = currentCourse.professor
            binding.tvID.text = currentCourse.id

            if (currentCourse.added) {
                binding.buttonID.setImageResource(R.drawable.ic_baseline_delete_24)

            } else {
                binding.buttonID.setImageResource(R.drawable.ic_baseline_add_24)
            }
            if (currentCourse.isOverlapping && showOverlapping) {
                binding.constraintlayoutIDcontainer.setCardBackgroundColor(getColor(context!!, R.color.overlapping))
            } else {
                binding.constraintlayoutIDcontainer.setCardBackgroundColor(getColor(context!!, R.color.nonoverlapping))
            }
            binding.buttonID.setOnClickListener {
                listener.onClick(holder, position)
            }
        }
    }

    override fun getItemCount() = courseList.size


    //Stuff for searching courses:
    override fun getFilter(): Filter {
        return object : Filter() {

            //Automatic on background thread
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                filterText = charSequence.toString()
                val filteredList = CourseData()

                if (charSequence.isEmpty()) {
                    filteredList.addAll(allCourses)
                } else {
                    for (course in allCourses) {
                        //Locale.ROOT as a param of toLowerCase was suggested by Android Studio
                        //since missing that is a common source of bugs?!
                        if (course.title.lowercase(Locale.ROOT)
                                .contains(charSequence.toString().lowercase(Locale.ROOT))
                        ) {
                            filteredList.add(course)
                        }
                    }
                }

                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            //Automatic on UI thread
            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                courseList.clear()
                courseList.addAll(filterResults.values as CourseData)

                notifyDataSetChanged()
            }
        }
    }
}