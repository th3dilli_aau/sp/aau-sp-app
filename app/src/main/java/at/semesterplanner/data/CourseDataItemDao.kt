package at.semesterplanner.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface CourseDataItemDao {
    @Query("SELECT * FROM courses")
    fun getAll(): Flow<List<CourseDataItem>>

    @Query("SELECT * FROM courses")
    fun getAllDirect(): List<CourseDataItem>

    @Insert
    fun insertAll(courses: List<CourseDataItem>)

    @Query("DELETE FROM courses")
    fun deleteAll()

    @Update
    fun update(vararg course: CourseDataItem)
}