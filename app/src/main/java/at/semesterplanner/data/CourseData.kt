package at.semesterplanner.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

class CourseData : ArrayList<CourseDataItem>()

@Entity(tableName = "courses")
data class CourseDataItem(
    @PrimaryKey @Expose
    val id: String,
    @Expose
    val endTime: Int,
    @Expose
    val professor: String,
    @Expose
    val startTime: Int,
    @Expose
    val sws: Double,
    @Expose
    val title: String,
    @Expose
    val type: String,
    @Expose
    val weekDay: String,
    @Expose
    val semester: String,
    @Expose(serialize = false, deserialize = false)
    var added: Boolean,
    @Expose(serialize = false, deserialize = false)
    var isOverlapping: Boolean

) {
    fun getTime(): String {
        return "$weekDay ${startTime / 60}:${
            String.format(
                "%02d",
                startTime % 60
            )
        } - ${endTime / 60}:${String.format("%02d", endTime % 60)}"
    }
}