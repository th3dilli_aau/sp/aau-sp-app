package at.semesterplanner.data

import android.content.Context
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.asLiveData
import at.semesterplanner.R
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.TimeUnit

class CourseRepository(private val courseDao: CourseDataItemDao, private val context: Context) {
    private val SERVER_URL = "https://api.planner.th3dilli.dev/coursesv2"


    val courses: Flow<List<CourseDataItem>>
        get() {
            if (courseDao.getAll().asLiveData().value == null || courseDao.getAll()
                    .asLiveData().value?.isEmpty() == true
            ) {
                CoroutineScope(Dispatchers.IO).launch {
                    updateData()
                }
            }
            return courseDao.getAll()
        }


    @WorkerThread
    fun insertAll(courses: List<CourseDataItem>) {
        courseDao.insertAll(courses)
    }

    @WorkerThread
    fun deleteAll() {
        courseDao.deleteAll()
    }

    @WorkerThread
    fun update(vararg course: CourseDataItem) {
        courseDao.update(*course)
    }

    //Fetch Data from backend
    private fun fetchData(): CourseData {
        val con = URL(SERVER_URL).openConnection() as HttpURLConnection

        val br = BufferedReader(InputStreamReader(con.inputStream))
        val gsonBuilder = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
        val newCourses = gsonBuilder.fromJson(br.readText(), CourseData::class.java)
        val oldCourses = courseDao.getAllDirect()

        // add new courses when updating
        for (course in oldCourses) {
            if (course.added) {
                val newCours = newCourses.find { cur -> cur.id == course.id }
                if (newCours != null) {
                    newCours.added = true
                }
            }
        }

        deleteAll()
        insertAll(newCourses)

        //updateTimestamp
        val sharedPref =
            context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putLong(
                context.getString(R.string.last_update_time),
                Calendar.getInstance().timeInMillis
            )
            apply()
        }
        return newCourses
    }


    private fun updateData() {
        val currentTimestamp: Long = Calendar.getInstance().timeInMillis
        val sharedPref =
            context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        val latestUpdateTime = sharedPref.getLong(context.getString(R.string.last_update_time), 0)

        val timeDifference = currentTimestamp - latestUpdateTime

        val maxTime = TimeUnit.DAYS.toMillis(1)

        if (timeDifference > maxTime) {
            Log.i("UPDATE_SP", "Time expired, Data updated!")
            fetchData()
        }
    }
}