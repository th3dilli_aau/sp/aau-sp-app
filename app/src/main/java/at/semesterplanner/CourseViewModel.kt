package at.semesterplanner

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import at.semesterplanner.data.CourseDataItem
import at.semesterplanner.data.CourseRepository


class CourseViewModel(private val repository: CourseRepository) : ViewModel() {
    val courses: LiveData<List<CourseDataItem>> = repository.courses.asLiveData()

    fun update(vararg course: CourseDataItem) {

            repository.update(*course)

    }
}

class CourseViewModelFactory(private val repository: CourseRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CourseViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CourseViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}